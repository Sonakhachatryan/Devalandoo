# Devalandoo

PHP/Laravel developer task

#Installation

<ul>
  <li>Clone project from gitlab: <a href="https://gitlab.com/Sonakhachatryan/Devalandoo.git">https://gitlab.com/Sonakhachatryan/Devalandoo.git</li>
  <li>Create virtual host</li>
  <li>
      <p>In project home directory run following commands</p>
      <ol>
        <li>composer install</li>
        <li>php artisan storage:link</li>
        <li>php artisan migrate</li>
        <li>gulp</li>
      </ol>
  </li>
</ul>
    