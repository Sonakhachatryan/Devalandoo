@extends('layouts.master')

@section('content')
    <div class="container-centered">
        <div class="card">
            <div class="card-header text-white bg-info">
                Login
                <small class="float-right"><a href="{{ url('password-reset') }}">Forgot password?</a></small>
            </div>
            <div class="card-body">
                @include('layouts.messages')
                <form method="post" action="{{ url('/login') }}">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{old("email")}}">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <div class="form-submit">
                        <button type="submit" class="btn btn-outline-primary">Login</button>
                        <small><a href="{{ url('/register') }}">Don't have an account? </a></small>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop