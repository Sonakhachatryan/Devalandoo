@extends('layouts.master')

@section('content')
    <div class="container-centered">
        <div class="card">
            <div class="card-header text-white bg-info">
                Register
            </div>
            <div class="card-body">
                @include('layouts.messages')
                <form method="post" action="{{ url('/register') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{old("name")}}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{old("email")}}">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password">
                    </div>
                    <div class="form-submit">
                        <button type="submit" class="btn btn-outline-primary">Register</button>
                        <small><a href="{{ url('/login') }}">Already have an account? </a></small>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop