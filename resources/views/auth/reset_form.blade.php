@extends('layouts.master')

@section('content')
    <div class="container-centered">
        <div class="card">
            <div class="card-header text-white bg-info">
                Reset password
            </div>
            <div class="card-body">
                @include('layouts.messages')
                <form method="post" action="{{ url('/reset') }}">
                    @csrf
                    <input name="token" type="hidden" value="{{ old('token') ? old('token') : $token }}">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm Password">
                    </div>
                    <div class="form-submit">
                        <button type="submit" class="btn btn-outline-primary">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop