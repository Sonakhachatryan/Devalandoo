@extends('layouts.master')

@section('content')
    <div class="container-centered">
        <div class="card">
            <div class="card-header text-white bg-info">
                Password reset
            </div>
            <div class="card-body">
                @include('layouts.messages')
                <form method="post" action="{{ url('/send-reset-link') }}">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{old("email")}}">
                    </div>
                    <div class="form-submit">
                        <button type="submit" class="btn btn-outline-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop