@extends('layouts.master')

@section('content')
    <div class="container-centered">
        <div class="card">
            <div class="card-header text-white bg-info">
                User data
            </div>
            <div class="card-body">
                @include('layouts.messages')
                Name : {{ $user->name  }}<br>
                Email: {{ $user->email }}
            </div>
        </div>
    </div>
@stop