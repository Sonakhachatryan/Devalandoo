@extends('layouts.master')

@section('content')
    <div class="container-centered">
        @include('layouts.messages')
        <form id="image-upload" method="post" action="{{ url('user/upload') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="btn btn-info" for="images">Choose file</label>
                <input class="d-none" type="file" name="images[]" id="images" multiple>
            </div>
        </form>
        <div id="uploads" class="row preview"></div>
        <div class="form-submit mb-5 d-none">
            <button form="image-upload" type="submit" class="btn btn-success">Save</button>
        </div>
        <h1>My images</h1>
        <div class="row preview">
            @foreach($user->images as $image)
                <div class="col-4">
                    <div class="img-wrapper">
                        <img src="{{ url('/storage/' . $image->path)  }}">
                        <div class="img-cover">
                            <button class="btn btn-danger" data-id="{{ $image->id }}">Delete</button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal fade" id="confirmModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Delete confirmation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Are you sure you want to delete?
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                    <a href="#"><button type="button" class="btn btn-danger">Delete</button></a>
                </div>

            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ url('js/app.min.js') }}"></script>
@stop