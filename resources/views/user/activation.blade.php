@extends('layouts.master')

@section('content')
    <div class="container-centered">
        @include('layouts.messages')
        <p>We have emailed you an activation link.Please check your email</p>
        <small><a href="{{ url('/user/send-link') }}">Don't get an email?</a></small>
    </div>
@stop