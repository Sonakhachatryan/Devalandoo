@extends('layouts.master')

@section('content')
    <div class="container-centered">
        <div class="card">
            <div class="card-header text-white bg-info">
                Change password
            </div>
            <div class="card-body">
                @include('layouts.messages')
                <form method="post" action="{{ url('/user/change-password') }}">
                    @csrf
                    <div class="form-group">
                        <label for="old_password">Old password</label>
                        <input type="password" name="old_password" class="form-control" id="old_password" aria-describedby="emailHelp" placeholder="Enter email" value="{{old("email")}}">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm Password">
                    </div>
                    <div class="form-submit">
                        <button type="submit" class="btn btn-outline-primary">Change Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop