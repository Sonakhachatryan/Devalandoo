/**
 * show uploaded images
 */
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = [];
        for(let i = 0; i < input.files.length; i++){
            $('#uploads').append('<div class="col-4"><img data-number="' + i +'"></div>');
            reader[i] = new FileReader();

            reader[i].onload = function(e) {
                $(document).find('img[data-number="' + i +'"]').attr('src', e.target.result);
            }

            reader[i].readAsDataURL(input.files[i]);
        }
    }

    $('.form-submit').removeClass('d-none');
}

/**
 * listen to file input change
 */
$("#images").change(function() {
    $('#uploads').empty();
    readURL(this);
});

/**
 * listen to delete button click
 */
$('.img-cover button').on('click', function () {
    $('#confirmModal a').attr('href', '/user/images/delete/' + $(this).data('id'));
    $('#confirmModal').modal('show');
})