<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::group(["middleware" => "authenticated:false"], function (){
   Route::get('/login', 'AuthController@showLoginForm');
   Route::post('/login', 'AuthController@login');
   Route::get('/register', 'AuthController@showRegistrationForm');
   Route::post('/register', 'AuthController@register');
   Route::get('/password-reset', 'AuthController@showEmailForm');
   Route::get('/reset/{token}', 'AuthController@showResetForm');
   Route::post('/reset', 'AuthController@reset');
   Route::post('/send-reset-link', 'AuthController@sendResetLink');
});

Route::group(["middleware" => "authenticated:true"], function (){
   Route::get('/logout', 'AuthController@logout');

   Route::group(["middleware" => "activated:false", 'prefix' => 'user'], function (){
       Route::get('activate', 'AuthController@showActivationView');
       Route::get('send-link', 'AuthController@sendActivationCode');
       Route::get('activate-account/{token}', 'AuthController@activate');
   });

   Route::group(["middleware" => "activated:true", 'prefix' => 'user'], function (){
       Route::get('account', 'UserController@account');
       Route::get('images', 'UserController@images');
       Route::get('images/delete/{id}', 'UserController@deleteImage');
       Route::post('upload', 'UserController@upload');
       Route::get('change-password', 'UserController@showPasswordChangeForm');
       Route::post('change-password', 'UserController@changePassword');
   });
});
