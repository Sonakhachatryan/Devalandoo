<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ActivationCode extends Model
{
    protected $table = 'activation_codes';

    public $timestamps = false;
    /**
     * Can be mass assigned
     *
     * @var array
     */
    protected $fillable = ['token', 'user_id', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
