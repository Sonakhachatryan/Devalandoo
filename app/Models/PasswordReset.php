<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $table = 'password_resets';

    public $timestamps = false;
    /**
     * Can be mass assigned
     *
     * @var array
     */
    protected $fillable = ['token', 'email', 'created_at'];
}
