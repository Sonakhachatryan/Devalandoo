<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $isAuthenticated)
    {

        if($isAuthenticated === "false" && Auth::check()){
           return redirect('/');
        }

        if($isAuthenticated === "true" && !Auth::check()){
           return redirect('/login');
        }

        return $next($request);
    }
}
