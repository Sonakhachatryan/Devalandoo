<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsActivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $activated)
    {
        if(Auth::user()->is_activated && $activated === "false"){
            return redirect('/');
        }

        if(!Auth::user()->is_activated && $activated === "true"){
            return redirect('user/send-token');
        }

        return $next($request);
    }
}
