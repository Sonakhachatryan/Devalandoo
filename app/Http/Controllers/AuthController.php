<?php

namespace App\Http\Controllers;

use App\Models\ActivationCode;
use App\Models\PasswordReset;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    /**
     * get login page view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * get registration page view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * login user
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public  function login(Request $request)
    {
       if(Auth::attempt($request->only('email', 'password'))){

           if(Auth::user()->is_activated){
               return redirect('/user/account');
           }

           return $this->sendActivationCode();
       }

       Session::flash('error', 'Invalid username or password provided.');
       return back();
    }

    /**
     * register user
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        Auth::login($user);
        return $this->sendActivationCode();
    }

    /**
     * return activation view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showActivationView()
    {
        return view('user.activation');
    }

    /**
     * Create activation link and send to user
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sendActivationCode()
    {
        $user = Auth::user();
        ActivationCode::where('user_id', $user->id)->delete();

        $code = new ActivationCode();
        $code->token = $this->generateRandomString() . time();
        $code->user_id = $user->id;
        $code->created_at = Carbon::now()->toDateTimeString();
        $code->save();

        Mail::to($user->email)->send(new \App\Mail\ActivationCode($user));

        return view('user.activation');
    }

    /**
     * Activate account if token is valid
     *
     * @param $token
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function activate($token)
    {
        $activationCode = ActivationCode::where('token', $token)->first();
        if(!$activationCode){
            Session::flash('error', 'Invalid token.We send you a new one.');
            return $this->sendActivationCode();
        }
        $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $activationCode->created_at);

        if($createdAt->diffInMinutes(Carbon::now()) > 120){
            Session::flash('error', 'Token expired.We send you a new one.');
            return $this->sendActivationCode();
        }

        $user = $activationCode->user;
        $user->is_activated = 1;
        $user->save();

        $activationCode->delete();
        if(!Auth::check()){
            Auth::login($user);
        }

        return redirect('/user/account');
    }

    /**
     * log out user
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    /**
     * generate random string
     *
     * @return string
     */
    private function  generateRandomString() {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 50; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }


    /**
     * show form for password reset email
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showEmailForm()
    {
        return view('auth.reset');
    }

    /**
     * show password reset link
     *
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm($token)
    {
        return view('auth.reset_form', compact('token'));
    }

    /**
     * Send password reset link
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLink(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if(!$user) {
            Session::flash('error', 'Email not found.');
            return back();
        }

        PasswordReset::where('email',$request->email)->delete();

        $code = new PasswordReset();
        $code->token = $this->generateRandomString() . time();
        $code->email = $request->email;
        $code->created_at = Carbon::now()->toDateTimeString();
        $code->save();

        Mail::to($user->email)->send(new \App\Mail\PasswordReset($code->token));

        Session::flash('success', 'We send you reset link.Please check the email.');
        return back();
    }

    /**
     * Reset password
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reset(Request $request)
    {
        $this->validate($request,[
            'password' => 'required|confirmed|min:6'
        ]);

        $passwordReset = PasswordReset::where('token', $request->token)->first();
        if(!$passwordReset){
            Session::flash('error', 'Invalid token.Please try again.');
            return redirect('/password-reset');
        }
        $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $passwordReset->created_at);

        if($createdAt->diffInMinutes(Carbon::now()) > 120){
            Session::flash('error', 'Token expired.Please try again.');
            return redirect('/password-reset');
        }

        $user = User::where('email', $passwordReset->email)->first();
        $user->password = Hash::make($request->password);
        $user->save();

        PasswordReset::where('token', $request->token)->delete();
        Auth::login($user);
        return redirect('/user/account');
    }

}
