<?php

namespace App\Http\Controllers;


use App\Models\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class UserController extends BaseController
{

    /**
     * get homepage view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function account()
    {
        return view('user.account');
    }

    /**
     * get activation view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getActivationView()
    {
        return view('user.activate');
    }

    /**
     * get password change view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPasswordChangeForm()
    {
        return view('user.password');
    }

    /**
     * chnage current user password
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePassword(Request $request)
    {
        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);

        if (!Hash::check($request->old_password, $this->user->password))
        {
            Session::flash('error', 'Wrong old password.');

            return back();
        }

        $this->user->password = Hash::make($request->password);
        $this->user->save();

        Session::flash('success', 'Password was changed successfully.');

        return redirect('/user/account');
    }

    /**
     * Return images view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function images()
    {
        return view('user.images');
    }

    /**
     * upload user images
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function upload(Request $request)
    {

        $this->validate($request,[
            'images.*' => 'image|max:1024'
        ],['images.*.image' => 'FIle type must be png, jpg, svg or gif.']);


        foreach($request->images as $image){
            $path = Storage::put('public/' . $this->user->id, $image);
            $path = str_replace('public/', '', $path);
            Images::create([
                'path' => $path,
                'user_id' =>  $this->user->id
            ]);
        }


        Session::flash('success', 'Files uploaded succesfully.');
        return back();
    }

    /**
     * remove image with given id
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteImage($id){

        $file = Images::find($id);
        Storage::delete('public/' . $file->path);
        Images::destroy($id);

        Session::flash('success', 'Image deleted successfully.');
        return back();
    }
}
