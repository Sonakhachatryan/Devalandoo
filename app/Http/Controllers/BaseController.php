<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if(Auth::check()){
                $this->user = Auth::user();
                view()->share('user', Auth::user());
            }

            return $next($request);
        });
    }
}
