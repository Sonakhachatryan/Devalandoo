<?php

namespace App\Http\Controllers;

class IndexController extends Controller
{
    /**
     * get homepage view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }
}
